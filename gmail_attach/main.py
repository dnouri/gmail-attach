import click

from .download import download
from .gmail import make_service


@click.group()
def cli():
    pass


@cli.command('download')
@click.option('--query',
              default="from:robot@awesome.io filename:xlsx")
@click.option('--filepattern',
              default='*')
@click.option('--output',
              default="gmail_attach_output")
@click.option('--credentials',
              default="credentials.json", type=click.Path(exists=True))
def download_cmd(query, filepattern, output, credentials):
    service = make_service(credentials=credentials)
    filenames_out = download(service, query, filepattern, output)
    print("Downloaded a total of {} files:\n{}".format(
        len(filenames_out), '\n'.join(filenames_out)))
