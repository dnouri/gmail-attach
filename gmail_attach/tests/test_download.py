import base64
from unittest.mock import MagicMock

import pytest


def make_mock_service(
    list_result,
    message_get_result=None,
    attachments_get_result=None,
):
    service = MagicMock()
    api = service.users().messages()
    api.list().execute.return_value = list_result
    api.get().execute.return_value = message_get_result
    api.attachments().get().execute.return_value = attachments_get_result
    return service


class TestDownload:
    @pytest.fixture
    def download(self):
        from gmail_attach.download import download
        return download

    def test_no_messages(self, download, tmpdir):
        service = make_mock_service(list_result={'messages': []})
        result = download(service, '', '*', str(tmpdir))
        assert result == []

    def test_no_parts(self, download, tmpdir):
        service = make_mock_service(
            list_result={
                'messages': [{'id': '123'}],
                },
            message_get_result={
                'payload': {},
                },
        )
        result = download(service, '', '*', str(tmpdir))
        assert result == []

    def test_no_filename_match(self, download, tmpdir):
        service = make_mock_service(
            list_result={
                'messages': [{'id': '123'}],
                },
            message_get_result={
                'payload': {
                    'parts': [
                        {
                            'body': {'attachmentId': '456'},
                            'filename': 'foo.txt',
                            },
                        ],
                    },
                },
        )
        result = download(service, '', 'bar*', str(tmpdir))
        assert result == []

    def test_no_attachment_id(self, download, tmpdir):
        service = make_mock_service(
            list_result={
                'messages': [{'id': '123'}],
                },
            message_get_result={
                'payload': {
                    'parts': [
                        {
                            'body': {},
                            'filename': 'foo.txt',
                            },
                        ],
                    },
                },
        )
        result = download(service, '', 'bar*', str(tmpdir))
        assert result == []

    def test_download(self, download, tmpdir):
        service = make_mock_service(
            list_result={
                'messages': [{'id': '123'}],
                },
            message_get_result={
                'payload': {
                    'parts': [
                        {
                            'body': {'attachmentId': '456'},
                            'filename': 'foo.txt',
                            },
                        ],
                    },
                },
            attachments_get_result={
                'data': base64.encodebytes(b'haha!').decode('ascii'),
                },
        )
        result = download(service, '', 'foo*', str(tmpdir))
        assert len(result) == 1
        assert open(result[0]).read() == 'haha!'
