from base64 import urlsafe_b64decode
from fnmatch import fnmatch
import os


def download(service, query, filepattern, output):
    os.makedirs(output, exist_ok=True)
    query = "has:attachment " + query

    filenames_out = []
    api = service.users().messages()
    page_token = None
    while True:
        results = api.list(
            userId='me', q=query, pageToken=page_token).execute()
        page_token = results.get('nextPageToken')
        for message_info in results['messages']:
            message_id = message_info['id']
            message = api.get(userId='me', id=message_id).execute()
            for part in message['payload'].get('parts', []):
                attachment_id = part['body'].get('attachmentId')
                if attachment_id is None:
                    continue
                filename = part['filename']
                if not fnmatch(filename, filepattern):
                    continue
                print("Downloading {} from {}...".format(filename, message_id))
                attachment = api.attachments().get(
                    userId='me',
                    messageId=message_id,
                    id=attachment_id,
                ).execute()
                b64data = attachment['data'].encode('ascii')
                data = urlsafe_b64decode(b64data)
                filename_dest = os.path.join(output, filename)
                with open(filename_dest, 'wb') as fi:
                    fi.write(data)
                filenames_out.append(filename_dest)
        if not page_token:
            break

    return filenames_out
