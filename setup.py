import os

from setuptools import find_packages
from setuptools import setup


version = '0.1a1'

install_requires = [
    'click',
    'google-api-python-client',
    'google_auth_oauthlib',
    ]

testing_requires = [
    'pytest',
    'pytest-cov',
    'pytest-flakes',
    ]

here = os.path.abspath(os.path.dirname(__file__))
README = CHANGES = ''
if os.path.exists(os.path.join(here, 'README.rst')):
    README = open(os.path.join(here, 'README.rst'), encoding='utf-8').read()
if os.path.exists(os.path.join(here, 'CHANGES.txt')):
    CHANGES = open(os.path.join(here, 'CHANGES.txt'), encoding='utf-8').read()


setup(name='gmail-attach',
      version=version,
      description='Download Gmail attachments using a CLI',
      long_description=README + '\n\n' + CHANGES,
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=install_requires,
      extras_require={
          'testing': testing_requires,
          'all': testing_requires,
          },
      entry_points={
          'console_scripts': [
              'gmail-attach = gmail_attach.main:cli',
              ],
          },
      )
