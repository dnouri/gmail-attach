FROM        python:3
COPY        . /app
WORKDIR     /app
RUN         pip install -U pip && pip install -r requirements.txt -e .
CMD         ["gmail-attach", "--help"]
